class StaticPagesController < ApplicationController

  def home
    ytclient = YouTubeIt::Client.new(:dev_key => ENV["YT_KEY"])
    if params[:search].present?
      result = Geocoder.coordinates(params[:search])
      if result.present?
        @instagram =  Instagram.media_search(result.first,result.last, {:count => 30})
        @twitter = $twitter.search("geocode:#{result.first},#{result.last},10km", :result_type => "recent").take(30)
     
        @coord1 = result.first
        @coord2 = result.last
        
        @newslocation = Geocoder.search(result).first
        @news = Feedjira::Feed.fetch_and_parse URI.encode("http://news.google.com/news?q=#{@newslocation.city}&output=rss")
        
        @flickrresults = $flickr.photos_search({"lat" => "#{result.first}", "lon" => "#{result.last}"})
        
        @ytresults = ytclient.videos_by(:location => "#{result.first},#{result.last}", :locationRadius => "10mi", :time => :this_week)
        
  
      else 
      
      end
    else
      result = Geocoder.coordinates("Chicago")
      if result.present?
        @instagram =  Instagram.media_search(result.first,result.last, {:count => 30})
   
        @twitter = $twitter.search("geocode:#{result.first},#{result.last},10km", :result_type => "recent").take(30)
        @coord1 = result.first
        @coord2 = result.last
        
        @newslocation = Geocoder.search(result).first
        @news = Feedjira::Feed.fetch_and_parse URI.encode("http://news.google.com/news?q=#{@newslocation.city}&output=rss")
        
        @flickrresults = $flickr.photos_search({"lat" => "#{result.first}", "lon" => "#{result.last}"})
        
        @ytresults = ytclient.videos_by(:location => "#{result.first},#{result.last}", :locationRadius => "10mi", :time => :this_week)
      
  
      else 
      
      end
    end
  end
end
